package com.spring.cloud.microservices.grettings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GrettingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrettingsApplication.class, args);
	}
}
