package com.spring.cloud.microservices.grettings.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Dani on 28/05/2017.
 */
@RestController
public class HelloController {

    @RequestMapping("/grettings")
    public String gretting() {
        return "Hello from EurekaClient!";
    }
}
